<?php
error_reporting(E_ALL ^ E_NOTICE);
session_start();

if(empty($_SESSION['user']->id)):
    header('LOCATION:index.php');
    exit;
endif;

//Load the database configuration file
include 'dbConfig.php';

// Texr eingeben Event
if(isset($_POST['abschicken']) && !empty($_POST['content'])):
    $query = "INSERT INTO antwort (user, flaschenpost, content, location, op) VALUES (".$_SESSION['user']->id.", ".$_GET['fid'].", '".$_POST['content']."', '".$_POST['location']."', 1)";

    if ($db->query($query) !== TRUE) {
        echo "Error: " . $query . "<br>" . $db->error;
    }
    //$db->close();
endif;

// Chatverlauf laden
$query = "SELECT * FROM antwort WHERE flaschenpost = ".$_GET['fid']."  order by date";
$objslist = $db->query($query);

$query = "SELECT * FROM flaschenpost WHERE id = ".$_GET['fid'];
$flaschenpostobj = $db->query($query);
$flaschenpost = $flaschenpostobj->fetch_object();
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <title>Lovebo.de</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window,document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '170965336587525');
        fbq('track', 'PageView');
        fbq('track', 'ViewContent');
    </script>
    <noscript>
        <img height="1" width="1" src="https://www.facebook.com/tr?id=170965336587525&ev=PageView&noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->

</head>


<body onload="findMe()">
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '1101314813333038',
            xfbml      : true,
            version    : 'v2.12'
        });

        FB.AppEvents.logPageView();
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDwQtj0GJYgS9Fafxkwn0iyWt5oRKfoEh0">
</script>
<script>
    "use strict";
    function findMe() {
        if(navigator.geolocation){
            navigator.geolocation.getCurrentPosition(geocodeLatLng);
        } else {
            alert('blaaa');
        }
    }


    function geocodeLatLng(position) {

        var geocoder = new google.maps.Geocoder;

        var latlng = {lat: position.coords.latitude, lng: +position.coords.longitude};
        geocoder.geocode({'location': latlng}, function(results, status) {
            if (status === 'OK') {
                console.info(results);
                if (results[3]) {
                    document.getElementById('geo_input').value = results[3].formatted_address;
                } else {
                    var span = document.querySelector('span.alert');
                    span.innerHTML = '<strong>Geolocation Fehler!</strong><br />' +
                        'Es ist ein Fehler bei der Standortbestimmung aufgetreten.<br />' +
                        'Bitte lade diese Seite neu! Ohne Standortbestiummung kannst du die Flaschenpost nicht absenden!<br/><strong>Es ist kein Rückschluss auf die genaue Position möglich!</strong>';
                    span.style.display = "block";
                }
            } else {
                var span = document.querySelector('span.alert');
                span.innerHTML = '<strong>Geolocation Fehler!</strong><br />' +
                    'Es ist ein Fehler bei der Standortbestimmung aufgetreten.<br />' +
                    'Bitte lade diese Seite neu! Ohne Standortbestiummung kannst du die Flaschenpost nicht absenden!<br/><strong>Es ist kein Rückschluss auf die genaue Position möglich!</strong>';
                span.style.display = "block";
            }
        });
    }
</script>
<header>
    <a href="start_page.php">
        <img src="icons_mobil/lovebo_ueberschrift_mobile.svg"/>
        <img src="lovebo_ueberschrift+logo_desktop.svg"/>
    </a>
</header>

<div class="fallingHearts">
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
</div>

<input type="checkbox" id="navchanger">
<nav>
    <ul id="menu_top">
        <li> <a href="new_message.php"><img src="iconsimg/neue_fp.svg"/> NEUE FLASCHENPOST</a></li>
        <li> <a href="my_messages.php"><img src="iconsimg/pfeil_eigene_fp.svg"/> EIGENE FLASCHENPOST</a></li>
        <li> <a href="received_message.php"><img src="iconsimg/pfeil_erhaltene_fp.svg"/> GEFUNDENE FLASCHENPOST</a></li>
    </ul>

    <ul id="menu_bottom">
        <!--li> <a href="settings.php"><img src="iconsimg/einstellungen.svg"/> EINSTELLUNGEN</a></li-->
        <li> <a href="index.php?action=logout"><img src="iconsimg/fb-art_sml.png"/> LOGOUT</a></li>
    </ul>

    <label for="navchanger"><img src="iconsimg/menue_desktop.svg"/></label>
</nav>

<main>
    <article id="chat">
        <h1><a href="my_messages.php"><img src="iconsimg/zurueck_pfeil.svg"/></a>Antwort aus <?php echo $flaschenpost->location?> vom <?php $date = date_create($flaschenpost->date); echo $date->format('d.m.Y')  ?></h1>
        <form action="chatverlauf_eigene.php?fid=<?php echo $_GET['fid']?>" method="post">
            <input type="checkbox" id="originalchanger">
            <div class="original_fp">
                <p><?php echo $flaschenpost->content ?></p>
                <div class="linear_gradient"></div>

                <label for="originalchanger"></label>
            </div>

            <div class="scroll-view">
                <div class="chatarea">

                    <?php
                    if ($objslist->num_rows > 0):
                        $last_date_day = "";
                        while($row = $objslist->fetch_object()):


                            $today = new DateTime();
                            $dateToCompare = date_create($row->date);

                            $interval = date_diff($dateToCompare, date_create($today->format('Y-m-d')));
                            if($interval->format('%R%a') == -0 )
                                $date_day = "Heute";
                            elseif ($interval->format('%R%a') == +0 )
                                $date_day = "Gestern";
                            else
                                $date_day = $dateToCompare->format('d.m.Y');

                            if($date_day != $last_date_day)
                                echo "<div class=\"day\">".$date_day."</div>";

                            $last_date_day = $date_day;
?>

                            <div class="<?php echo empty($row->op)? 'other' : 'self'?>">
                                <p><?php echo $row->content?></p>
                                <time><img src="iconsimg/uhr_chat.svg"/><?php $date = date_create($row->date); echo $date->format('H:i')?></time>
                            </div>

                        <?php
                        endwhile;
                    endif;
                    ?>
                </div>
            </div>

            <div class="textarea">
                <textarea name="content" placeholder="Hier Text eingeben..." maxlength="240" spellcheck="true"></textarea>
                <input type="submit" name="abschicken" value="" />
                <input name="location" id="geo_input" type="hidden" value="" />
            </div>
        </form>
    </article>
</main>

<footer>
    <div id="imp">
        <a class="one" href="impressum.php">Impressum</a>
        <a class="one" href="nutzungsbedingungen.php">Nutzungsbedingungen</a>
        <a class="two" href="datenschutz.php">Datenschutz</a>
    </div>
    <div class="made">© 2017 LOLLEBO - Made in Dresden</div>
</footer>

</body>
</html>